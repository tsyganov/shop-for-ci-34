#!/bin/bash
set -e
REL_PATH=`dirname $0`
cd $REL_PATH
CURRENT_DIR=`pwd`

cd $CURRENT_DIR/../api

NODE_ENV=test yarn run seed

pm2 start "yarn run start:test" --name="shop-api-test"

cd $CURRENT_DIR/../frontend

pm2 start "yarn run start:test" --name="shop-front-test"

echo $CURRENT_DIR
cd $CURRENT_DIR

while ! nc -z localhost 3010; do
  sleep 0.1 # wait for 1/10 of the second before check again
done

set +e
yarn start
EXIT_CODE=$?
set -e

pm2 kill

exit $EXIT_CODE
