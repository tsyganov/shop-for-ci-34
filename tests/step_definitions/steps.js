const { I } = inject();
// Add in your custom step files

Given('я нахожусь на странице регистрации', () => {
  I.amOnPage('register');
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
  I.waitForElement('#' + fieldName, 30);
  I.fillField('#' + fieldName, text);
});

When('нажимаю на кнопку {string}', buttonName => {
  I.click(buttonName);
});

Then('я вижу текст {string}', text => {
  I.waitForText(text);
});
