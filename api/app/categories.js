const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Category = require('../models/Category');
const Product = require('../models/Product');

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const categories = await Category.find();

    const productsCount = await Product.aggregate([{$group : { _id : '$category', count : {$sum : 1}}}]);

    const categoriesWithCount = categories.map(category => {
      const categoryCount = productsCount.find(pc => pc._id.equals(category._id)).count;
      return {
        ...category.toObject(),
        count: categoryCount
      }
    });

    res.send(categoriesWithCount);
  } catch (e) {
    console.log(e);
    res.sendStatus(500)
  }
});

router.post('/', [auth, permit('admin')], (req, res) => {
  const category = new Category(req.body);

  category.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

module.exports = router;
